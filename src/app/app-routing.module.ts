
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProductosComponent} from 'src/app/components/productos/productos.component';
import {ClientesComponent} from 'src/app/components/clientes/clientes.component';
import {VentasComponent} from 'src/app/components/ventas/ventas.component';
import {TiendaComponent} from 'src/app/components/tienda/tienda.component';
import {AgregarProductoComponent} from "src/app/components/agregar-producto/agregar-producto.component";
import {DetalleDeProductoComponent} from "src/app/components/detalle-de-producto/detalle-de-producto.component";
import {TerminarCompraComponent} from "src/app/components/terminar-compra/terminar-compra.component";
import {DetalleDeVentaComponent} from "src/app/components/detalle-de-venta/detalle-de-venta.component";
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  {path: 'productos', component: ProductosComponent, canActivate: [AuthGuard] },
  {path: 'productos/agregar', component: AgregarProductoComponent, canActivate: [AuthGuard] },
  {path: 'clientes', component: ClientesComponent, canActivate: [AuthGuard] },
  {path: 'ventas', component: VentasComponent, canActivate: [AuthGuard] },
  {path: 'tienda', component: TiendaComponent},
  {path: 'producto/detalle/:id', component: DetalleDeProductoComponent},
  {path: 'terminar_compra', component: TerminarCompraComponent, canActivate: [AuthGuard] },
  {path: 'detalle-venta/:id', component: DetalleDeVentaComponent, canActivate: [AuthGuard] },
  {path: '', redirectTo: "/tienda", pathMatch: "full"},
  {path: '**', redirectTo: "/tienda"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  /*imports: [RouterModule.forRoot(routes, {
    useHash: true, // <- Indicar que se use el hash
  })],*/

  exports: [RouterModule]
})
export class AppRoutingModule {
}
