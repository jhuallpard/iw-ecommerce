
import {Component, OnInit} from '@angular/core';
import {CarritoService} from "src/app/service/carrito.service";
import {DataSharingService} from "src/app/service/data-sharing.service";
// Auth0
import { AuthService } from '@auth0/auth0-angular';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  profileJson: string = null;
  title = 'e-commerce-angular-node';
  public productos = [];

  siteLanguage = 'Español';

  languageList = [
    { code: 'es', label: 'Español' },
    { code: 'en', label: 'English' },
    { code: 'pt', label: 'Português' }
  ];

  constructor(private carritoService: CarritoService, private dataSharingService: DataSharingService, public auth: AuthService, private service: TranslocoService) {
    // Comunicación entre componentes
    this.dataSharingService.currentMessage.subscribe(mensaje => {
      if (mensaje == "car_updated") {
        this.refrescarCarrito();
      }
    })
  }

  public async refrescarCarrito() {
    this.productos = await this.carritoService.obtenerProductos();
  }

  public total() {
    // Quién te conoce reduce
    let total = 0;
    this.productos.forEach(p => total += p.precio);
    return total;
  }

  ngOnInit(): void {
    this.refrescarCarrito();
    this.auth.user$.subscribe(
      (profile) => (this.profileJson = JSON.stringify(profile, null, 2))
    );
  }

  registrarse(){
    this.auth.loginWithRedirect({ login_hint: 'signup' });
  }

  loginWithRedirect(): void {
    // Call this to redirect the user to the login page
    this.auth.loginWithRedirect({ login_hint: "signup" });
  }

  logout(): void {
    // Call this to log the user out of the application
    this.auth.logout({ returnTo: window.location.origin });
  }

  changeSiteLanguage(language: string): void {
    this.service.setActiveLang(language);
    this.siteLanguage = this.languageList.find(f => f.code === language).label;
  }

}
